﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Los números pares del 23 al 7 son: ");
            var a = 23;
            //var b = 7;
            while (a>7)
            {
                a= (a - 1);
                var c = a % 2;
                if (c==0)
                {
                    Console.WriteLine(a);
                }
            }
            Console.ReadKey();
        }
    }
}
