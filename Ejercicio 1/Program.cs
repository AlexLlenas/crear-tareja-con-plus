﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crear_tareja_con_plus
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inserte un número entero:");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Su valor absoluto es: ");
            if (n>=0)
            {
                Console.WriteLine(n);
            }
            else
            {
                Console.WriteLine(n * -1);
            }
            Console.ReadKey();
        }
    }
}
