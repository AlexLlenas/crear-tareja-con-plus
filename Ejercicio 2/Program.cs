﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inserte dos números enteros:");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("El mayor de los dos números es:");
            int[] c = { a, b };
            Console.WriteLine(c.Max());
            Console.ReadKey();
        }
    }
}
