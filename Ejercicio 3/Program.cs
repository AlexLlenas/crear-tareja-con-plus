﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Teclee algo");
            char a = Convert.ToChar(Console.ReadLine());
            Console.Write("Lo que acaba de teclear es ");
            if (char.IsPunctuation(a))
            {
                Console.Write("un signo de puntuación");
            }
            else if (char.IsLetter(a))
            {
                Console.Write("una letra");
            }
            else if ((char.IsDigit(a)))
            {
                Console.Write("un número");
            }
            else if (char.IsSymbol(a))
            {
                Console.Write("un símbolo matemático");
            }
            else
            {
                Console.Write("indetectable");
            }



                Console.ReadKey();
        }
    }
}
