﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escriba una letra o número");
            char a = Convert.ToChar(Console.ReadLine());
            string[] b = { "a", "e", "i", "o", "u" };
            if (char.IsDigit(a))
            {
                Console.WriteLine("Acaba de escribir un número");
            }
            else if (a.Equals("a"))
            {
                Console.WriteLine("La letra es una vocal");
            }
            else
            {
                Console.WriteLine("Escribió una consonante");
            }
            //por ahora está incompleto

            Console.ReadKey();
        }
    }
}
